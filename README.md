

# [Setting Up](https://stackoverflow.com/questions/2411031/how-do-i-clone-into-a-non-empty-directory)

```
cd ~
git init
git remote add origin https://gitlab.com/ryanwhite04/workspace.git
git fetch
git reset origin/master
git checkout -t origin/master
git submodule update --init --recursive
```

# NVM, Node, and NPM (also PNPM)

To upgrade, run the same script again,
but instead of v0.33.11 uses the latest tagged release from [here](https://github.com/creationix/nvm)

```
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install node
npm install -g pnpm
```

# Hugo

```
sudo apt install hugo
```

You will probably have the following output of ```cat /etc/os-release```
```
PRETTY_NAME="Debian GNU/Linux 9 (stretch)"
NAME="Debian GNU/Linux"
VERSION_ID="9"
VERSION="9 (stretch)"
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
```

More about [Debian 9](https://www.debian.org/News/2017/20170617)

- Apache 2.4.25
- Asterisk 13.14.1
- Chromium 59.0.3071.86
- Firefox 45.9 (in the firefox-esr package)
- GIMP 2.8.18
- an updated version of the GNOME desktop environment 3.22
- GNU Compiler Collection 6.3
- GnuPG 2.1
- Golang 1.7
- KDE Frameworks 5.28, KDE Plasma 5.8, and KDE Applications 16.08 and 16.04 for PIM components
- LibreOffice 5.2
- Linux 4.9
- MariaDB 10.1
- MATE 1.16
- OpenJDK 8
- Perl 5.24
- PHP 7.0
- PostgreSQL 9.6
- Python 2.7.13 and 3.5.3
- Ruby 2.3
- Samba 4.5
- systemd 232
- Thunderbird 45.8
- Tomcat 8.5
- Xen Hypervisor
- the Xfce 4.12 desktop environment
- more than 51,000 other ready-to-use software packages, built from a bit more of 25,000 source packages.

## Packages & Versions

- apt: 1.4.6
- kernel: Linux 4.9
- gcc: 6.3
- Gnome: 3.22
- KDE: Plasma 5.8 LTS
- Xfce: 4.12
- libc: libc6 2.24-11